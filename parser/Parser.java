package parser;

import common.Lexeme;
import common.SymbolTable;

import java.util.*;
import java.util.regex.*;

/**
 * Created by Viktor on 24.04.2015.
 */
public class Parser {
    SymbolTable st;
    Scanner sc;
    String prog = "";
    public Parser(SymbolTable st, Scanner sc) {
        this.st = st;
        this.sc = sc;
    }

    public List<Lexeme> parse() {
        List<Lexeme> list = new LinkedList<Lexeme>();
        char [] arrayOfChars = prog.toCharArray();

        /* --- Магия регулярных выражений --- */
        Pattern prePat = Pattern.compile("^[A-Za-z]$");
        Pattern postPat = Pattern.compile("^[A-Za-z0-9]$"); // для проверки, что это переменная
        Pattern opPat = Pattern.compile("^[-|+|/|*]");
        Pattern intPat = Pattern.compile("^[0-9]");
        /* --- /Магия регулярных выражений --- */

        for(int i = 0; i<arrayOfChars.length; i++) {
            if(arrayOfChars[i] == ':'  && arrayOfChars[i+1]!= '=') {
                System.out.println("После знака : должно быть =, оно не найдено. Проверьте код программы");

                return null;
            }
            else if(prePat.matcher(String.valueOf(arrayOfChars[i])).matches()){
                String var = String.valueOf(arrayOfChars[i]);
                i++;
                while(postPat.matcher(String.valueOf(arrayOfChars[i])).matches()) {
                    var += arrayOfChars[i];
                    i++;
                }
                Lexeme thisLexeme = new Lexeme();
                thisLexeme.type = Lexeme.Type.VAR;
                thisLexeme.extra = st.getIndex(var);

                list.add(thisLexeme);
                i--;
            }
            else if(arrayOfChars[i] == ':' && arrayOfChars[i+1] == '=') {
                Lexeme thisLexeme = new Lexeme();
                thisLexeme.type = Lexeme.Type.ASSIGN;

                list.add(thisLexeme);
                i++;
            }
            else if(opPat.matcher(String.valueOf(arrayOfChars[i])).matches()){
                Lexeme thisLexeme = new Lexeme();
                switch (arrayOfChars[i]) {
                    case '+':
                        thisLexeme.type = Lexeme.Type.PLUS;
                        thisLexeme.extra = 0;
                        list.add(thisLexeme);
                        break;
                    case '-':
                        thisLexeme.type = Lexeme.Type.SUB;
                        thisLexeme.extra = 0;
                        list.add(thisLexeme);
                        break;
                    case '/':
                        thisLexeme.type = Lexeme.Type.DIV;
                        thisLexeme.extra = 1;
                        list.add(thisLexeme);
                        break;
                    case '*':
                        thisLexeme.type = Lexeme.Type.MULT;
                        thisLexeme.extra = 1;
                        list.add(thisLexeme);
                        break;
                }
            }
            else if(intPat.matcher(String.valueOf(arrayOfChars[i])).matches()){
                String num = "";
                while(intPat.matcher(String.valueOf(arrayOfChars[i])).matches()) {
                    num+=arrayOfChars[i];
                    i++;
                }
                int resnum = Integer.parseInt(num);
                i--;
                Lexeme thisLexeme = new Lexeme();
                thisLexeme.type = Lexeme.Type.INT;
                thisLexeme.extra = resnum;
                list.add(thisLexeme);
            }
            else if(arrayOfChars[i] == ';') {
                Lexeme thisLexeme = new Lexeme();
                thisLexeme.type = Lexeme.Type.SEMICOLON;
                list.add(thisLexeme);
            }
            else if(arrayOfChars[i] == '.') {// нашли точку - закончили анализ проги
                Lexeme thisLexeme = new Lexeme();
                thisLexeme.type = Lexeme.Type.DOT;
                list.add(thisLexeme);
//                for(Lexeme k: list) {
//                    String var = "";
//                    if(k.type == Lexeme.Type.VAR)
//                        var = "\t\t\tVAR: "+st.getName(k.extra);
//                    System.out.println("TYPE: "+k.type+"\t\t\tEXTRA: "+k.extra+var);
//                }
                return list;
            }
        }
        return null;
    }

    public void read() {
        while(sc.hasNext())
            prog += sc.nextLine();
    }
}
