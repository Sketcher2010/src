package interpreter.commands;

import common.SymbolTable;
import interpreter.Command;

/**
 * Created by Viktor on 24.04.2015.
 */
public class AssignCmd extends Command {
    @Override
    public void run(SymbolTable st) {
        st.setValue(getResult(), st.getValue(getFirstOperand()));
    }
}
