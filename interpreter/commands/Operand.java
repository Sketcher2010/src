package interpreter.commands;

import common.Lexeme;
import common.SymbolTable;
import interpreter.Var;

/**
 * Created by Viktor on 24.04.2015.
 */
public class Operand {
    public Lexeme.Type type;
    public Var var;

    public Operand(Lexeme.Type type, String name) {
        Var var = new Var();
        var.name = name;
        this.type = type;
        this.var = var;
    }

    public Operand(Lexeme lex, SymbolTable st) {
        Var var = new Var();
        this.type = lex.type;
        if(lex.type == Lexeme.Type.VAR)
            var.name = st.getName(lex.extra);
        else
            var.value = lex.extra;
        this.var = var;
    }

    public Operand(){}
}

