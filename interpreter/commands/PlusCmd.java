package interpreter.commands;

import common.SymbolTable;
import interpreter.Command;

/**
 * Created by Aliya on 07.05.2015.
 */
public class PlusCmd extends Command {
    @Override
    public void run(SymbolTable st) {
        st.setValue(getResult(), st.getValue(getFirstOperand())+st.getValue(getSecondOperand()));
    }
}
