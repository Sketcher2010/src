package interpreter.commands;

import common.SymbolTable;
import interpreter.Command;

/**
 * Created by Viktor on 10.05.2015.
 */
public class SubCmd extends Command{

    @Override
    public void run(SymbolTable st) {
        st.setValue(getResult(), st.getValue(getFirstOperand())-st.getValue(getSecondOperand()));
    }
}
