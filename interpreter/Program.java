package interpreter;

import common.SymbolTable;

import java.io.*;
import java.util.*;

/**
 * Created by Viktor on 24.04.2015.
 */
public class Program {
    private List<String> input;
    private List<Command> commands;
    private Scanner sc;
    private SymbolTable st;

    public Program(List<String> input, List<Command> commands, Scanner sc, SymbolTable st) {
        this.input = input;
        this.commands = commands;
        this.sc = sc;
        this.st = st;
    }

    public void run() {
        for(String var: input) {
            System.out.print(var + " = ");
            st.setValue(var, sc.nextInt());
        }
        for(Command command: commands) {
            command.run(st);

//            for(Var x: st.getVars()) {
//                System.out.println("VAR: "+x.name+"\t\tVAL: "+x.value);
//            }
//            System.out.println("===========================================");
        }
    }

    public SymbolTable getTable() {
        return st;
    }
}
