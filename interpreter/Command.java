package interpreter;

import common.SymbolTable;
import interpreter.commands.Operand;

/**
 * Created by Viktor on 24.04.2015.
 */
public abstract class Command {
    private Operand firstOperand;
    private Operand secondOperand;
    private Operand result;

    public void setResult(Operand result) {
        this.result = result;
    }


    public void setSecondOperand(Operand secondOperand) {
        this.secondOperand = secondOperand;
    }

    public void setFirstOperand(Operand firstOperand) {

        this.firstOperand = firstOperand;
    }

    public String getResult() {return result.var.name;}
    public Operand getFirstOperand() {return firstOperand;}
    public Operand getSecondOperand() {return secondOperand;}


    public abstract void run(SymbolTable st);
}
