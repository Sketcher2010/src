package syntax;

import common.*;
import interpreter.*;
import interpreter.commands.*;

import java.io.OutputStream;
import java.util.*;

/**
 * Created by Aliya on 24.04.2015.
 */

public class Syntax {
    public static Program analyze(SymbolTable st, List<Lexeme> lexemes) throws SyntaxException {
        LinkedList<String> assignedVars = new LinkedList<String>();
        LinkedList<String> input = new LinkedList<String>();
        LinkedList<Command> commands = new LinkedList<Command>();
        LinkedList<LinkedList<Lexeme>> rpn = new LinkedList<LinkedList<Lexeme>>();
        ListIterator<Lexeme> lexemeListIterator = lexemes.listIterator();

        while (lexemeListIterator.hasNext()) {                      //List of lexemes to RPN
            Lexeme lex = lexemeListIterator.next();
            if (lex.type == Lexeme.Type.ASSIGN) {
                lexemeListIterator.previous();
                Lexeme prev = lexemeListIterator.previous();
                if (prev.type == Lexeme.Type.VAR)
                    assignedVars.add(st.getName(prev.extra));
                else throw new SyntaxException();
                LinkedList<Lexeme> subList = new LinkedList<Lexeme>();
                Stack<Lexeme> temp = new Stack<Lexeme>();
                lexemeListIterator.next();
                lexemeListIterator.next();
                lex = lexemeListIterator.next();
                while (lex.type != Lexeme.Type.SEMICOLON && lexemeListIterator.hasNext()) {
                    switch (lex.type) {
                        case VAR:
                            if (!assignedVars.contains(st.getName(lex.extra))&&!input.contains(st.getName(lex.extra)))
                                input.add(st.getName(lex.extra));
                            subList.add(lex);
                            break;
                        case INT:
                            subList.add(lex);
                            break;
                        case DIV:
                        case SUB:
                        case MULT:
                        case PLUS:
                            while (!temp.empty() && temp.peek().extra >= lex.extra)
                                subList.add(temp.pop());
                            temp.push(lex);
                            break;
                        default:
                            throw new SyntaxException();
                    }
                    lex = lexemeListIterator.next();
                }
                while (!temp.empty())
                    subList.add(temp.pop());
                rpn.add(subList);
            }
        }

        for (LinkedList<Lexeme> subList : rpn) {                    //RPN to list of three-address commands
            subList.toString();
            Stack<Operand> tempStack = new Stack<Operand>();
            int indexOfTempVar = 0;
            Command cmd;
            for (Lexeme lex : subList) {
                if (lex.type == Lexeme.Type.INT || lex.type == Lexeme.Type.VAR) {
                    tempStack.push(new Operand(lex, st));
                }
                else {
                    Operand tempVar = new Operand(Lexeme.Type.VAR, "_" + indexOfTempVar++);

                    switch (lex.type) {
                        case PLUS:
                            cmd = new PlusCmd();
                            cmd.setResult(tempVar);
                            break;
                        case SUB:
                            cmd = new SubCmd();
                            cmd.setResult(tempVar);
                            break;
                        case MULT:
                            cmd = new MultCmd();
                            cmd.setResult(tempVar);
                            break;
                        case DIV:
                            cmd = new DivCmd();
                            cmd.setResult(tempVar);
                            break;
                        default:
                            throw new SyntaxException();
                    }
                    cmd.setSecondOperand(tempStack.pop());
                    cmd.setFirstOperand(tempStack.pop());
                    commands.add(cmd);
                    tempStack.add(tempVar);
                }

            }
            Command assignCmd = new AssignCmd();
            assignCmd.setFirstOperand(tempStack.pop());
            assignCmd.setSecondOperand(null);
            assignCmd.setResult(new Operand(Lexeme.Type.VAR, assignedVars.get(rpn.indexOf(subList)))); //Get the name of variable that needs to be assigned by the number or line
            commands.add(assignCmd);
        }
        return new Program(input, commands, new Scanner(System.in), st);
    }
}
