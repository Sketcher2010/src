package common;

import interpreter.Var;
import interpreter.commands.Operand;

/**
 * Created by Viktor on 20.04.2015.
 */
public class Lexeme {
    public enum Type {
        VAR,
        INT,
        ASSIGN,
        SEMICOLON,
        PLUS,
        SUB,
        MULT,
        DIV,
        DOT
    };
    public Type type;
    public int extra;

    public Operand toOperand(SymbolTable st) {
        Operand op = new Operand();
        op.type = type;
        op.var = new Var();
        if (op.type == Type.VAR) {
            op.var.name = st.getName(extra);
            op.var.value = st.getValue(st.getName(extra));
        } else {
            op.var.value = extra;
        }
        return op;
    }
}
