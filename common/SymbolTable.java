package common;

import interpreter.Var;
import interpreter.commands.Operand;

import java.util.LinkedList;

/**
 * Created by Viktor on 24.04.2015.
 */
public class SymbolTable {
    LinkedList<Var> st;
    public SymbolTable() {
        st = new LinkedList<Var>();
    }

    public LinkedList<Var> getVars() {
        return st;
    }

    public int getValue(String name) {
       return st.get(getIndex(name)).value;
    }
    public int getValue(Operand op) {
        if(op.type == Lexeme.Type.VAR)
            return st.get(getIndex(op.var.name)).value;
        else
            return op.var.value;
    }
    public void setValue(String name, int val) {
        st.get(getIndex(name)).value = val;
    }

    public int getIndex(String name) {
        int ind = 0;
        for(Var x: st) {
            if(x.name.equals(name))
                return ind;
            ind++;
        }
        return this.setNewVar(name);
    }
    public int setNewVar(String name) {
        if(name != null) {
            Var val = new Var();
            val.name = name;
            st.add(val);
        }
        return st.size()-1;
    }

    public String getName(int index) {
        return st.get(index).name;
    }


}
