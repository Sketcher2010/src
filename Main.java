import java.io.*;
import java.util.*;

import common.Lexeme;
import common.SymbolTable;
import parser.Parser;
import interpreter.*;
import syntax.*;
import syntax.SyntaxException;

/**
 * Created by Viktor on 19.04.2015.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException, SyntaxException {

        SymbolTable st = new SymbolTable();

        Parser parser = new Parser(st, new Scanner(new FileInputStream("input.txt")));
        parser.read();

        List<Lexeme> lexemes = parser.parse();


        Program program = Syntax.analyze(st, lexemes);
        program.run();

        SymbolTable result = program.getTable();
        for (Var var : result.getVars()) {
            System.out.println(var.name + " = " + var.value);
        }
    }
}
